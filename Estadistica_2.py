import math
def relative_frecuency (L, letter):
    '''(str, str)->int
Return the times that one letter repeats :
    >>> count_a_letter ("contraccion", "c")
    >>> 3
    >>> count_a_letter ("abanicar", "a")
    >>>3
    '''
    repetitions=0
    for i in range (len (L)):
        if L[i]==letter:
            repetitions=repetitions+1
    return repetitions
def average (L):
    average=0
    for i in range (len (L)):
        average=average+ L[i]
    return average/len (L)
def s_square (L):
    '''Varianza'''
    varianza=0
    for i in range (len (L)):
        varianza=varianza+(L[i]-average(L))**2
    return varianza/len(L)
def s (L):
    '''desviacion estandar'''
    s=math.sqrt (s_square (L))
    return s
def cuasi_varianza (L):
    return (len (L)/(len (L)-1))*s_square(L)
def CV (L):
    analisis=''
    CV= cuasi_varianza(L)/average (L)
    if CV< 0.08:
        analisis= 'homogeneo'
    elif 0.08< CV< 0.18:
        analisis= 'moderadamente homogeneo'
    elif 0.18< CV< 0.3:
        analisis= 'moderadamente hetereogeneo'
    else:        
        analisis= 'heterogeneo'
    return str (CV)+ ' es '+ analisis
def identify_variable (L):
    variables=[]
    for element in L:
        if not element in variables:
            variables.append (element)
    print  (variables)
def Percentil (L, r):
    i=((len (L))*r)
    percentil=0
    if type(i)== type (3):
        percentil= L[i]
    else:
        i_new= int (i-1)
        percentil= (L[i_new]+ L[i_new+1])/2
    return percentil
def desviacion_estandar (L):
    var=0
    med = average (L)
    n= len (L)
    for element in L:
        var = var+ (element-med)**2
    return math.sqrt (var/n)
def intervalos_de_confianza (L, confianza):
    med= average (L)
    n= len (L)
    SD= desviacion_estandar (L)
    if confianza == 90:
        minimo= med-1.64*(SD/math.sqrt(n))
        maximo= med+1.64*(SD/math.sqrt(n))
    elif confianza == 95:
        minimo= med-1.96*(SD/math.sqrt(n))
        maximo= med+1.96*(SD/math.sqrt(n))
    elif confianza == 99:
        minimo= med-2.58*(SD/math.sqrt(n))
        maximo= med+2.58*(SD/math.sqrt(n))
    print (minimo,"<= X <=",maximo)
def pre_desviacion (L):
    var=0
    med = average (L)
    for element in L:
        var = var+ (element-med)**2
    return var
    
        

